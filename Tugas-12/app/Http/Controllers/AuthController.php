<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('page.register');
    }

    public function home(Request $request){
        //bisa gunakan salah satu cara dibawah
        $firstName = $request['firstname'];
        $lastName = $request->input('lastname');

        return view('page.home', ['fname' => $firstName, 'lname' => $lastName]);
    }
}

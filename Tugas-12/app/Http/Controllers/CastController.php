<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class CastController extends Controller
{
    public function create(){
        return view('cast.create');
    }

    public function store(Request $request){

        //validasi inputan
        $request->validate([
            'nama' => 'required|max:40|min:1',
            'umur' => 'required',
            'bio' => 'required|max:255|min:10'
        ]);

        //insert data ke database
        DB::table('cast')->insert([
        'nama' => $request->input('nama'),
        'umur' => $request->input('umur'),
        'bio' => $request->input('bio')
        ]);

        //redirect => jika data sudah ditambahkan akan diarahkan ke /cast
        return redirect('/cast');
    }

    public function index(){
        $cast = DB::table('cast')->get();

        return view('cast.alldata', ['cast' => $cast]); //cast.alldata = ../view/cast/alldata.blade.php
    }


    // $id adalah parameter yang diterima
    public function show($id){
        $castdetail = DB::table('cast')->find($id);
        return view('cast.detail', ['castdetail' => $castdetail]);
    }

    public function edit($id){
        $castedit = DB::table('cast')->find($id);
        return view('cast.edit', ['castedit' => $castedit]);
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required|max:40|min:1',
            'umur' => 'required',
            'bio' => 'required|max:255|min:10'
        ]);

        DB::table('cast')
        ->where('id', $id)
        ->update(
            [
                'nama' => $request->input('nama'),
                'umur' => $request->input('umur'),
                'bio' => $request->input('bio')
            ]
            );

        //redirect => jika data sudah ditambahkan akan diarahkan ke /cast
        return redirect('/cast');
    }

    public function destroy($id){
        DB::table('cast')->where('id', $id) -> delete();

        return redirect('/cast');
    }
}

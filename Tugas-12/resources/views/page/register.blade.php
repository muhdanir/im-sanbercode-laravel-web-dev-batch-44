@extends('layout.master')
@section('title')
Halaman Registrasi
@endsection
@section('sub-title')
Registrasi
@endsection
@section('content')
<h1>Buat Account Baru!</h1>
<h3>Sign Up form</h3>
<form action="/home" method="POST">
    @csrf
    <label>First Name :</label><br>
    <input type="text" name="firstname"><br><br>

    <label>Last Name :</label><br>
    <input type="text" name="lastname"><br><br>

    <label>Gender :</label><br>
    <input type="radio" name="gender" value="Male">Male<br>
    <input type="radio" name="gender" value="Female">Female<br>
    <input type="radio" name="gender" value="Other">Other<br><br>

    <label>Nationality :</label>
    <select name="nationality">
        <option value="Indonesian">Indonesian</option>
        <option value="English">English</option>
        <option value="Other">Other</option>
    </select><br><br>

    <label>Language Spoken :</label><br>
    <input type="checkbox" name="skill" value="Bahasa Indonesia">Bahasa Indonesia<br>
    <input type="checkbox" name="skill" value="English">English<br>
    <input type="checkbox" name="skill" value="Other">Other<br><br>

    <label>Bio :</label><br>
    <textarea name="message" rows="'10" cols="30"></textarea><br><br>

    <input type="submit" value="kirim">
</form>
@endsection

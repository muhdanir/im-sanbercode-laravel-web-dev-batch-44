@extends('layout.master')
@section('title')
Halaman Data Cast
@endsection
@section('sub-title')
Data Cast
@endsection
@section('content')
<div class="jumbotron">
    <h1 class="display-4">{{$castdetail->nama}}</h1>
    <p class="lead">{{$castdetail->umur}} Tahun</p>
    <hr class="my-4">
    <p>{{$castdetail->bio}}</p>
</div>
<a class="btn btn-primary" href="/cast" role="button">Kembali</a>
@endsection
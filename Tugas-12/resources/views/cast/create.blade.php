@extends('layout.master')
@section('title')
Halaman Membuat Cast
@endsection
@section('sub-title')
Cast
@endsection
@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama</label>
      <input type="text" class="form-control @error('nama') is-invalid @enderror" name='nama' placeholder="Masukkan Nama">
    </div>
    @error('nama')
    <div class="alert alert-danger" role="alert">
        {{$message}}
      </div>
    @enderror
    <div class="form-group">
        <label>Umur</label>
        <input type="number" class="form-control  @error('umur') is-invalid @enderror" name='umur' placeholder="Masukkan Umur">
      </div>
      @error('umur')
      <div class="alert alert-danger" role="alert">
          {{$message}}
        </div>
      @enderror
    <div class="form-group">
      <label>Bio</label>
      <textarea name='bio' class='form-control  @error('bio') is-invalid @enderror' id='' cols='30' rows='10' placeholder="Jelaskan tentang diri anda...."></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger" role="alert">
        {{$message}}
      </div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection
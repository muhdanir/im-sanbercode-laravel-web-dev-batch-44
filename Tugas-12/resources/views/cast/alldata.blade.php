@extends('layout.master')
@section('title')
Halaman Semua Data Cast
@endsection
@section('sub-title')
Semua Data Cast
@endsection
@section('content')
<a class="btn btn-primary" href="/cast/create" role="button">Tambahkan Cast</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Detail</th>
      </tr>
    </thead>
    <tbody>
        {{-- forelse => jika data terdapat pada database maka akan ditampilkan jika tidak maka akan menampilkan yang empty --}}
        @forelse ($cast as $key => $datacast)
        <tr>
            <th scope="row">{{$key + 1}}</th>
            <td>{{$datacast->nama}}</td>
            <td>{{$datacast->umur}}</td>
            <td>
                <form action="/cast/{{$datacast->id}}" method="POST">
                <a class="btn btn-info" href="/cast/{{$datacast->id}}" role="button">Detail</a>
                <a class="btn btn-secondary" href="/cast/{{$datacast->id}}/edit" role="button">Edit</a>
                
                    @csrf
                    @method('delete')
                    <input type="submit" class="btn btn-danger" value="Hapus">
                </form>
            </td>
          </tr>
        @empty
        <tr>
            <td>
                Data Cast Kosong
            </td>
        </tr>
        @endforelse

    </tbody>
  </table>
@endsection
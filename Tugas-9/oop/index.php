<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Array</title>
</head>
<body>
    <?php
        require_once('Animal.php');
        require_once('Frog.php');
        require_once('Ape.php');

        $sheep = new Animal("Shaun");
        echo "name : " . $sheep->get_name() . "<br>";
        echo "legs : " . $sheep->get_legs() . "<br>";
        echo "cold blooded : " . $sheep->get_cold_blooded() . "<br><br>";

        $kodok = new Frog("Buduk");
        echo "name : " . $kodok->get_name() . "<br>";
        echo "legs : " . $kodok->get_legs() . "<br>";
        echo "cold blooded : " . $kodok->get_cold_blooded() . "<br>";
        echo "jump : " . $kodok->get_jump() . "<br><br>";

        $sungokong = new Ape("Kera Sakti");
        echo "name : " . $sungokong->get_name() . "<br>";
        echo "legs : " . $sungokong->get_legs() . "<br>";
        echo "cold blooded : " . $sungokong->get_cold_blooded() . "<br>";
        echo "yell : " . $sungokong->get_yell() . "<br>";
    ?>

</body>
</html>